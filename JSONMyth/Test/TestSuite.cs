using System;

using System.Collections.Generic;

namespace JSONMyth
{
	public class TestSuite
	{
		public List<Test> Tests { get; set; }
		public bool StopOnAssert { get; set; }

		public TestSuite()
		{
			Tests = new List<Test>();
			StopOnAssert = false;
		}

		public void AddTest( Test test )
		{
			Tests.Add( test );
		}

		public void RunTests()
		{
			System.Console.WriteLine( "Running Tests" );

			foreach ( Test test in Tests )
			{
				System.Console.Write( "[" + test.TestName + "] Result: " ); 

				test.RunTest();

				if ( test.DidAssert == false )
				{
					Console.ForegroundColor = ConsoleColor.Green;
					Console.Write( "PASSED\n" );
					Console.ResetColor();
				}
				else
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.Write( "FAILED" );

					foreach ( String message in test.Failures )
					{
						Console.WriteLine( "---> " + message );
					}
				}

				if ( StopOnAssert && test.DidAssert )
				{
					break;
				}
			}

			System.Console.WriteLine( "Finished Tests" );
		}
	}
}

