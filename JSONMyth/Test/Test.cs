using System;

using System.Collections.Generic;

namespace JSONMyth
{
	public class Test
	{
		protected String _TestName = "default";

		public String TestName { get { return _TestName; } }

		public List<String> Failures = new List<string>();

		public bool DidAssert { get { return Failures.Count > 0; } }

		public Test()
		{

		}

		protected void Assert( bool statement, String descriptor )
		{
			if ( statement )
			{
				return;
			}

			Failures.Add( descriptor );
		}

		public virtual void RunTest()
		{

		}
	}
}

