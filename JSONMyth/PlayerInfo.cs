using System;

using System.Collections.Generic;

namespace JSONMyth
{
	public class PlayerInfo
	{
		public Dictionary<String, SpaceShipInfo> Ships { get; set; }
		public String LastUsedShip { get; set; }

		public PlayerInfo()
		{
		}
	}
}

