using System;

namespace JSONMyth
{
	public class JSONPrimitive : JSONEntity
	{
		public String value;
		
		public JSONPrimitive()
		{
			
		}

		override public String ToString()
		{
			return value;
		}
	}
}

