using System;
using System.Collections.Generic;

namespace JSONMyth
{
	public class GlobalConfig
	{
		public Dictionary<String, String> Bindings { get; set; }
		
		public SpaceShipInfo DefaultShipInfo { get; set; }
		
		public GlobalConfig()
		{
		}
	}
}

