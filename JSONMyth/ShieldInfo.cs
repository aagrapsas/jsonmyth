using System;

namespace JSONMyth
{
	public class ShieldInfo
	{
		public int MaxShields { get; set; }
		public float RechargeRate { get; set; }
		public float RechargeDelaySeconds { get; set; }
		public String Name { get; set; }

		public ShieldInfo()
		{
		}
	}
}

