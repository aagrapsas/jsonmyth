using System;

using System.Collections.Generic;

namespace JSONMyth
{
	public class JSONArray : JSONEntity
	{
		public List<JSONEntity> Array = new List<JSONEntity>();
		
		public JSONArray()
		{
		}

		override public JSONEntity this[ int index ]
		{
			get
			{
				if ( Array.Count <= index )
				{
					return null;
				}

				return Array[ index ];
			}
		}
	}
}

