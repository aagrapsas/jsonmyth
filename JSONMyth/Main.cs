using System;
using System.IO;
using System.Collections.Generic;

namespace JSONMyth
{
	class MainClass
	{
		public static void Main( string[] args )
		{
			TestSuite suite = new TestSuite();
			suite.AddTest( new SpaceShipInfoTest() );

			suite.RunTests();
		}
	}
}
