using System;

namespace JSONMyth
{
	public class SpaceShipInfoTest : Test
	{
		public SpaceShipInfoTest()
		{
			this._TestName = "SpaceShipInfo Test";
		}

		override public void RunTest()
		{
			String json = "{ \"Armor\" : \"base armor\", \"Sensor\" : \"base sensor\", \"Shield\" : \"base shield\", \"Hull\" : \"fighter\", \"Engine\" : \"base engine\", \"EquippedWeapons\" : { \"Primary\" : [ \"gun\" ] }";

			SpaceShipInfo info = RunOn( json );

			String outputJson = JSONParser.ToJSON<SpaceShipInfo>( info );

			RunOn( outputJson );
		}

		private SpaceShipInfo RunOn( String json )
		{
			SpaceShipInfo info = JSONParser.FromJSON<SpaceShipInfo>( json );

			Assert( info != null, "Info null" );
			Assert( info.Armor == "base armor", "Base armor not found" );
			Assert( info.Sensor == "base sensor", "Base sensor not found" );
			Assert( info.Shield == "base shield", "Base shield not found" );
			Assert( info.Hull == "fighter", "Base hull not found" );
			Assert( info.Engine == "base engine", "Base engine not found" );
			Assert( info.EquippedWeapons.Count > 0, "No equipped weapons" );
			Assert( info.EquippedWeapons.ContainsKey( "Primary" ), "No primary key on weapons" );
			Assert( info.EquippedWeapons[ "Primary" ].Count > 0, "Primary key contains no weapons" );
			Assert( info.EquippedWeapons[ "Primary" ][ 0 ] == "gun", "Primary key did not have a gun as a weapon" );

			return info;
		}
	}
}

