using System;

using System.Collections.Generic;

namespace JSONMyth
{
	public class Test
	{
		public TestSub name { get; set; }
		public List<int> collection { get; set; }
		public Dictionary<String, TestSub> subs { get; set; }

		public Test()
		{
			subs = new Dictionary<String, TestSub>();
		}
	}
}

