using System;

using System.Collections.Generic;

namespace JSONMyth
{
	public class JSONEntity
	{
		public String Key = null;
		public JSONEntity Parent = null;
		public Dictionary<String, JSONEntity> Children = new Dictionary<String, JSONEntity>();
		
		public JSONEntity()
		{
			
		}

		virtual public JSONEntity this[ String key ]
		{
			get
			{
				if ( Children.ContainsKey( key ) == false )
				{
					return null;
				}

				return Children[ key ];
			}
		}

		virtual public JSONEntity this [ int index ]
		{
			get
			{
				return null;
			}
		}
	}
}

