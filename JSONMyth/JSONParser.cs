using System;

using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace JSONMyth
{
	public class JSONParser
	{
		public JSONParser()
		{
			
		}

		private static bool IsPrimitiveType( Type type )
		{
			if ( type.IsPrimitive || type == typeof( Decimal ) || type == typeof( String ) )
			{
				return true;
			}

			return false;
		}

		private static Object GetPrimitiveCast( String data, Type type )
		{
			if ( type == typeof( String ) )
			{
				return data;
			}

			Object[] param = new Object[ 1 ];
			param[ 0 ] = data;

			MethodInfo method = type.GetMethod( "Parse", new Type[] { typeof( string ) } );
			return method.Invoke( null, param );
		}

		private static void GenerateList( Type arguementType, ref System.Collections.IList list, JSONArray subArray )
		{
			if ( subArray == null )
			{
				return;
			}

			if ( IsPrimitiveType( arguementType ) )
			{
				// Iteratively add cast primitives to array
				for ( int i = 0; i < subArray.Array.Count; i++ )
				{
					list.Add( GetPrimitiveCast( ( ( JSONPrimitive )subArray[ i ] ).value, arguementType ) );
				}
			}
			else
			{
				MethodInfo method = typeof( JSONParser ).GetMethod( "DeserializeType" );
				MethodInfo genericMethod = method.MakeGenericMethod( arguementType );

				// Iterately add finalized objects to array
				for ( int i = 0; i < subArray.Array.Count; i++ )
				{
					object[] arrayParam = new object[ 1 ];
					arrayParam[ 0 ] = subArray.Array[ i ];
					list.Add( genericMethod.Invoke( null, arrayParam ) );
				}
			}
		}

		private static void GenerateDictionary( Type arguementType, ref System.Collections.IDictionary dictionary, JSONEntity subEntity )
		{
			if ( IsPrimitiveType( arguementType ) )
			{
				foreach ( String key in subEntity.Children.Keys )
				{
					JSONPrimitive primitive = ( JSONPrimitive )subEntity.Children[ key ];
					dictionary[ key ] = GetPrimitiveCast( primitive.value, arguementType );
				}
			}
			else
			{
				foreach ( String key in subEntity.Children.Keys )
				{
					MethodInfo method = typeof( JSONParser ).GetMethod( "DeserializeType" );
					MethodInfo genericMethod = method.MakeGenericMethod( arguementType );

					object[] entityParam = new object[ 1 ];
					entityParam[ 0 ] = subEntity.Children[ key ];

					dictionary[ key ] = genericMethod.Invoke( null, entityParam );
				}
			}
		}

		public static T DeserializeType<T>( JSONEntity entity ) where T : new()
		{
			T value = new T();

			Type type = typeof( T );

			// Handle generics
			if ( type.IsGenericType )
			{
				if ( type.GetGenericTypeDefinition() == typeof( Dictionary<,> ) )
				{
					// Dictionary
					Type arguementType = type.GetGenericArguments()[ 1 ];
					Type combinedType = typeof( Dictionary<,> ).MakeGenericType( new Type[] { typeof( string ), arguementType } );

					System.Collections.IDictionary dictionary = ( System.Collections.IDictionary )Activator.CreateInstance( combinedType );

					GenerateDictionary( arguementType, ref dictionary, entity );

					return ( T )dictionary;
				}
				else if ( type.GetGenericTypeDefinition() == typeof( List<> ) )
				{
					// List
					Type arguementType = type.GetGenericArguments()[ 0 ];
					Type combinedType = typeof( List<> ).MakeGenericType( new Type[] { arguementType } );

					System.Collections.IList list = ( System.Collections.IList )Activator.CreateInstance( combinedType );

					GenerateList( arguementType, ref list, entity as JSONArray );

					return ( T )list;
				}
			}

			PropertyInfo[] properties = type.GetProperties();

			foreach ( PropertyInfo info in properties )
			{
				// Do not attempt to write to read only
				if ( info.CanWrite == false )
				{
					continue;
				}

				if ( entity.Children.ContainsKey( info.Name ) == false )
				{
					continue;
				}

				JSONEntity subEntity = entity[ info.Name ];

				Type propertyType = info.PropertyType;

				if ( IsPrimitiveType( propertyType ) )
				{
					JSONPrimitive primitive = ( JSONPrimitive )subEntity;

					// Immediately stuff type in
					info.SetValue( value, GetPrimitiveCast( primitive.value, propertyType ), null );
				}
				else
				{
					Type listType = typeof( List<> );

					if ( propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == listType )
					{
						if ( ( subEntity is JSONArray ) == false )
						{
							continue;
						}

						// Handle array
						JSONArray subArray = (JSONArray)subEntity;

						Type arguementType = propertyType.GetGenericArguments()[ 0 ];

						Type combinedType = listType.MakeGenericType( arguementType );

						// Generate generic untyped list (that can be cast to typed)
						System.Collections.IList list = ( System.Collections.IList )Activator.CreateInstance( combinedType );

						GenerateList( arguementType, ref list, subArray );

						// Set the list property to the newly crafted instance
						info.SetValue( value, list, null );
					}
					else if ( propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof( Dictionary<,> ) )
					{
						// Dictionary
						Type arguementType = propertyType.GetGenericArguments()[ 1 ];
						Type combinedType = typeof( Dictionary<,> ).MakeGenericType( new Type[] { typeof( string ), arguementType } );

						System.Collections.IDictionary dictionary = ( System.Collections.IDictionary )Activator.CreateInstance( combinedType );

						GenerateDictionary( arguementType, ref dictionary, subEntity );

						info.SetValue( value, dictionary, null );
					}
					else
					{
						MethodInfo method = typeof( JSONParser ).GetMethod( "DeserializeType" );
						MethodInfo genericMethod = method.MakeGenericMethod( propertyType );

						object[] entityParam = new object[ 1 ];
						entityParam[ 0 ] = subEntity;

						// Object: deserialize the typed object
						info.SetValue( value, genericMethod.Invoke( null, entityParam ), null );
					}
				}
			}

			return value;
		}

		public static T FromJSON<T>( String json ) where T : new()
		{
			JSONEntity jsonObject = Deserialize( json );

			return DeserializeType<T>( jsonObject );
		}
		
		public static JSONEntity Deserialize( String json )
		{
			JSONEntity rootObject = null;
			
			List<JSONEntity> stack = new List<JSONEntity>();

			StringBuilder stringKey = new StringBuilder();;
			StringBuilder stringValue = new StringBuilder();
			bool wasStringFinished = true;
			bool wasLastCharacterEscape = false;
			
			bool isKey = false;
			
			foreach ( Char c in json )
			{		
				JSONEntity current = null;
				
				if ( stack.Count > 0 )
				{
					current = stack[ stack.Count - 1 ];
				}
				
				if ( rootObject == null && current != null )
				{
					rootObject = current;
				}
				
				if ( c == '\\' )
				{
					wasLastCharacterEscape = true;
				}
				else if ( ( c == '\n' || c == '\r' || c == '\t' ) && wasLastCharacterEscape == false )
				{
					continue;
				}
				else if ( c == '\"' && wasLastCharacterEscape == false)
				{
					wasStringFinished = !wasStringFinished;

					if ( isKey )
					{
						isKey = false;
					}
					else if ( stringValue.Length == 0 && stringKey.Length == 0 )
					{
						isKey = true;
					}
					else if ( stringValue.Length > 0 )
					{
						JSONPrimitive primitive = new JSONPrimitive();
						primitive.value = stringValue.ToString();
						stringValue.Length = 0;
						
						AddChild( stringKey.ToString(), current, primitive );
					}
				}
				else if ( wasStringFinished )
				{
					if ( c == ':' )
					{
						isKey = false;
					}
					else if ( c == '{' )
					{
						JSONEntity newObject = new JSONEntity();
						
						AddChild( stringKey.ToString(), current, newObject );
						
						stack.Add( newObject );
						
						stringKey.Length = 0;
						stringValue.Length = 0;
					}
					else if ( c == '[' )
					{
						JSONArray newArray = new JSONArray();
						
						AddChild( stringKey.ToString(), current, newArray );
						
						stack.Add( newArray );
						
						stringKey.Length = 0;
						stringValue.Length = 0;
					}
					else if ( c == '}' || c == ']' )
					{
						if ( stringValue.Length > 0 )
						{
							// Final primitive element of an array or object
							JSONPrimitive primitive = new JSONPrimitive();
							primitive.value = stringValue.ToString();

							AddChild( stringKey.ToString(), current, primitive );
						}
						else if ( current is JSONArray && stringKey.Length > 0 )
						{
							// Final primitive element of an array
							JSONPrimitive primitive = new JSONPrimitive();
							primitive.value = stringKey.ToString();

							AddChild( null, current, primitive );
						}

						stack.RemoveAt( stack.Count - 1 );

						stringKey.Length = 0;
						stringValue.Length = 0;
					}
					else if ( c == ',' )
					{
						if ( stringValue.Length > 0 )
						{
							// Value is part of array or entity
							JSONPrimitive primitive = new JSONPrimitive();
							primitive.value = stringValue.ToString();

							AddChild( stringKey.ToString(), current, primitive );
						}

						stringKey.Length = 0;
						stringValue.Length = 0;
					}
					else if ( isKey == false && c != ' ' )
					{
						// Non-string value as part of a key-value
						stringValue.Append( c );
					}
				}
				else
				{
					if ( isKey )
					{
						stringKey.Append( c );
					}
					else
					{
						stringValue.Append( c );
					}

					wasLastCharacterEscape = false;
				}
			}

			return rootObject;
		}
		
		private static void AddChild( String key, JSONEntity parent, JSONEntity child )
		{
			if ( parent == null )
			{
				return;
			}
			
			if ( parent is JSONArray )
			{
				JSONArray array = parent as JSONArray;
				array.Array.Add( child );
			}
			else
			{
				parent.Children.Add( key, child );
			}

			child.Key = key;
			
			child.Parent = parent;
		}

		public static String ToJSON<T>( T instance )
		{
			Type type = typeof( T );

			StringBuilder json = new StringBuilder();

			// Special case, passed a dictionary up front
			if ( type.IsGenericType && type.GetGenericTypeDefinition() == typeof( Dictionary<,> ) )
			{
				json.Append( "{ " );

				System.Collections.IDictionary dictionary = ( System.Collections.IDictionary )instance;

				Type dictionaryType = type.GetGenericArguments()[ 1 ];

				bool isPrimitive = IsPrimitiveType( dictionaryType );
				bool isFirst = true;

				foreach ( String key in dictionary.Keys )
				{
					if ( isFirst )
					{
						isFirst = false;
					}
					else
					{
						json.Append( ", " );
					}

					if ( isPrimitive )
					{
						json.Append( "\"" + key + "\" : " + dictionary[ key ].ToString() );
					}
					else
					{
						MethodInfo method = typeof( JSONParser ).GetMethod( "ToJSON" );
						MethodInfo genericMethod = method.MakeGenericMethod( dictionaryType );

						object[] entityParam = new object[ 1 ];
						entityParam[ 0 ] = dictionary[ key ];

						json.Append( "\"" + key + "\" : " + genericMethod.Invoke( null, entityParam ) );
					}
				}

				json.Append( " }" );

				return json.ToString();
			}
			else if ( type.IsGenericType && type.GetGenericTypeDefinition() == typeof( List<> ) )
			{
				json.Append( "[ " );

				Type listArguement = type.GetGenericArguments()[ 0 ];

				System.Collections.IList list = (System.Collections.IList)instance;

				bool isFirst = true;
				bool isPrimitive = IsPrimitiveType( listArguement );

				foreach ( Object element in list )
				{
					if ( isFirst )
					{
						isFirst = false;
					}
					else
					{
						json.Append( ", " );
					}

					if ( isPrimitive )
					{
						if ( listArguement == typeof( String ) )
						{
							json.Append( "\"" + element.ToString() + "\" " );
						}
						else
						{
							json.Append( element.ToString() );
						}
					}
					else
					{
						MethodInfo method = typeof( JSONParser ).GetMethod( "ToJSON" );
						MethodInfo genericMethod = method.MakeGenericMethod( listArguement );

						object[] entityParam = new object[ 1 ];
						entityParam [ 0 ] = element;

						json.Append( genericMethod.Invoke( null, entityParam ) );
					}
				}

				json.Append( "] " );

				return json.ToString();
			}

			json.Append( "{ " );

			PropertyInfo[] properties = type.GetProperties();

			bool isFirstProperty = true;

			foreach ( PropertyInfo property in properties )
			{
				if ( isFirstProperty )
				{
					isFirstProperty = false;
				}
				else
				{
					json.Append( ", " );
				}

				String propertyName = property.Name;

				json.Append( "\"" + propertyName + "\" : " );

				Type propertyType = property.PropertyType;
				object value = property.GetValue( instance, null );

				// Strings
				if ( propertyType == typeof( String ) )
				{
					json.Append( "\"" + value + "\" " );
					continue;
				}

				// Primitives (int, float, etc.)
				if ( IsPrimitiveType( propertyType ) )
				{
					json.Append( value.ToString() );
					continue;
				}

				Type listType = typeof( List<> );

				// Handle lists
				if ( propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == listType )
				{
					json.Append( "[ " );

					Type listArguement = propertyType.GetGenericArguments() [ 0 ];

					System.Collections.IList list = (System.Collections.IList)value;

					bool isFirst = true;
					bool isPrimitive = IsPrimitiveType( listArguement );

					foreach ( Object element in list )
					{
						if ( isFirst )
						{
							isFirst = false;
						}
						else
						{
							json.Append( ", " );
						}

						if ( isPrimitive )
						{
							if ( listArguement == typeof( string ) )
							{
								json.Append( "\"" + element.ToString() + "\" " );
							}
							else
							{
								json.Append( element.ToString() );
							}
						}
						else
						{
							MethodInfo method = typeof( JSONParser ).GetMethod( "ToJSON" );
							MethodInfo genericMethod = method.MakeGenericMethod( listArguement );

							object[] entityParam = new object[ 1 ];
							entityParam [ 0 ] = element;

							json.Append( genericMethod.Invoke( null, entityParam ) );
						}
					}

					json.Append( "] " );
				}
				else if ( propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof( Dictionary<,> ) )
				{
					// Handle dictionary
					json.Append( "{ " );

					System.Collections.IDictionary dictionary = (System.Collections.IDictionary)value;

					Type dictionaryType = propertyType.GetGenericArguments() [ 1 ];

					bool isPrimitive = IsPrimitiveType( dictionaryType );
					bool isFirst = true;

					foreach ( String key in dictionary.Keys )
					{
						if ( isFirst )
						{
							isFirst = false;
						}
						else
						{
							json.Append( ", " );
						}

						if ( isPrimitive )
						{
							json.Append( "\"" + key + "\" : " + dictionary [ key ].ToString() );
						}
						else
						{
							MethodInfo method = typeof( JSONParser ).GetMethod( "ToJSON" );
							MethodInfo genericMethod = method.MakeGenericMethod( dictionaryType );

							object[] entityParam = new object[ 1 ];
							entityParam [ 0 ] = dictionary [ key ];

							json.Append( "\"" + key + "\" : " + genericMethod.Invoke( null, entityParam ) );
						}
					}

					json.Append( "} " );
				}
				else
				{
					// An object type
					MethodInfo method = typeof( JSONParser ).GetMethod( "ToJSON" );
					MethodInfo genericMethod = method.MakeGenericMethod( propertyType );

					object[] entityParam = new object[ 1 ];
					entityParam [ 0 ] = value;

					json.Append( genericMethod.Invoke( null, entityParam ) + " " );
				}
			}

			json.Append( "}" );

			return json.ToString();
		}
	}
}

