using System;
using System.Collections.Generic;

namespace JSONMyth
{
	public class SpaceShipInfo
	{
		public String ShipName { get; set; }
		public String Armor { get; set; }
		public String Hull { get; set; }
		public String Engine { get; set; }
		public String Shield { get; set; }
		public String Sensor { get; set; }

		public Dictionary<String, List<String>> EquippedWeapons { get; set; }
		
		public SpaceShipInfo()
		{
			EquippedWeapons = new Dictionary<string, List<string>>();
		}
	}
}

